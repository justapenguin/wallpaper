// +build js

package wallpaper

import "errors"

func getMonitors() ([]*Monitor, error) {
	return nil, errors.New("wallpaper: getMonitors is not supported for platform: js")
}
