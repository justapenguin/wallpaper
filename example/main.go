package main

import (
	"fmt"

	"bitbucket.org/justapenguin/wallpaper"
)

func main() {
	background, err := wallpaper.Get()

	if err != nil {
		panic(err)
	}

	monitors, err := wallpaper.GetMonitors()

	if err != nil {
		panic(err)
	}

	for _, monitor := range monitors {
		fmt.Printf("Monitor: %s, %dx%d @ %d, %d\n", monitor.ID, monitor.Width, monitor.Height, monitor.PosX, monitor.PosY)
	}

	fmt.Println("Current wallpaper:", background)
	wallpaper.SetFromFile("/usr/share/backgrounds/gnome/adwaita-day.jpg")
}
