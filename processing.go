// +build !js

package wallpaper

import (
	"bufio"
	"image"
	"image/draw"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"os"
	"path/filepath"
	"runtime"
	"sync"

	"github.com/flopp/go-findfont"
	"github.com/fogleman/gg"
	"github.com/nfnt/resize"
	"github.com/oliamb/cutter"
	"github.com/pkg/errors"
)

var fontPath string

var fonts = map[string]string{
	"windows": "Verdana.ttf",
	"darwin":  "Verdana.ttf",
	"linux":   "Ubuntu.ttf",
}

func init() {

	font, ok := fonts[runtime.GOOS]

	if ok {
		// error is ignored as we can't import logging libraries
		fontPath, _ = findfont.Find(font)
	}
}

// gcd returns the greatest common divisor of two numbers
func gcd(x, y int) int {
	for y != 0 {
		x, y = y, x%y
	}

	return x
}

var tmpPathMutex = &sync.Mutex{}

func makeMultiWall(monitors []*Monitor, imagesInfo []*MonitorImage, overlay bool) (string, int, error) {
	totalWidth, totalHeight := 0, 0

	for _, monitor := range monitors {
		newWidth := monitor.PosX + monitor.Width
		if newWidth >= totalWidth {
			totalWidth = newWidth
		}
		newHeight := monitor.PosY + monitor.Height
		if newHeight >= totalHeight {
			totalHeight = newHeight
		}
	}

	// Create a rectangle to hold all the images
	r := image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: totalWidth, Y: totalHeight}}
	rgba := image.NewRGBA(r)

	for index, monitor := range monitors {
		err := processMonitorImage(monitor, imagesInfo[index], rgba, overlay)

		if err != nil {
			return "", index, err
		}
	}

	// Don't allow another call of the function to try to open tmp.jpg whilst it is being used
	tmpPathMutex.Lock()
	defer tmpPathMutex.Unlock()

	cacheDir, err := getCacheDir()

	if err != nil {
		return "", -1, errors.Wrapf(err, "unable to get cache dir")
	}

	tmpPath := filepath.Join(cacheDir, "multi-wall.jpg")

	outImage, err := os.Create(tmpPath)

	if err != nil {
		return "", -1, errors.Wrapf(err, "unable to create temp path at: %s", tmpPath)
	}

	defer outImage.Close()

	output := bufio.NewWriter(outImage)

	err = jpeg.Encode(output, rgba, &jpeg.Options{Quality: 100})

	if err != nil {
		return "", -1, errors.Wrap(err, "unable to encode multi-monitor image")
	}

	return tmpPath, -1, err
}

func processMonitorImage(monitor *Monitor, imageInfo *MonitorImage, drawImage draw.Image, overlay bool) error {
	// Open the images, these should be loaded from the database based on screen resolutions found
	monitorImage, err := os.Open(imageInfo.ImagePath)

	if err != nil {
		return errors.Wrapf(err, "unable to open image file: %s", monitorImage)
	}

	defer monitorImage.Close()

	input := bufio.NewReader(monitorImage)

	decodedImage, _, err := image.Decode(input)

	if err != nil {
		return ErrDecodeImage
	}

	if runtime.GOOS == "darwin" {
		// darwin does not need to take into account posX
		m := *monitor
		m.PosX, m.PosY = 0, 0
		*monitor = m
	}

	// Resize the image to the dimensions of the monitor
	if imageInfo.Mode == Stretch {
		decodedImage = resize.Resize(uint(monitor.Width), uint(monitor.Height), decodedImage, resize.Lanczos3)
	}

	// Resize the image to the dimensions of the monitor whilst maintaining aspect ratio
	if imageInfo.Mode == Fit || imageInfo.Mode == FitCenter {
		decodedImage = resize.Resize(uint(monitor.Width), 0, decodedImage, resize.Lanczos3)

		if decodedImage.Bounds().Max.Y > monitor.Height {
			decodedImage = resize.Resize(0, uint(monitor.Height), decodedImage, resize.Lanczos3)
		}
	}

	// Resize the image to fill the monitor whilst maintaining aspect ratio
	if imageInfo.Mode == Fill || imageInfo.Mode == FillCenter {
		decodedImage = resize.Resize(uint(monitor.Width), 0, decodedImage, resize.Lanczos3)

		if decodedImage.Bounds().Max.Y < monitor.Height {
			decodedImage = resize.Resize(0, uint(monitor.Height), decodedImage, resize.Lanczos3)
		}
	}

	// Set the position of the image (top left)
	x := monitor.PosX
	y := monitor.PosY

	canvasWidth := monitor.Width
	canvasHeight := monitor.Height
	cropMode := cutter.TopLeft

	// Keep the image size but position it in the center of the canvas
	if imageInfo.Mode == Center || imageInfo.Mode == FitCenter || imageInfo.Mode == FillCenter {

		cropMode = cutter.Centered

		if decodedImage.Bounds().Max.X < monitor.Width {
			x = monitor.Width - (((monitor.Width) - (decodedImage.Bounds().Max.X)) / 2) - decodedImage.Bounds().Max.X + monitor.PosX
		}

		if decodedImage.Bounds().Max.X > monitor.Width {
			x = monitor.Width - (((decodedImage.Bounds().Max.X) - (monitor.Width)) / 2) - monitor.Width + monitor.PosX
			canvasWidth = decodedImage.Bounds().Max.X - (((decodedImage.Bounds().Max.X) - (monitor.Width)) / 2)
		}

		if decodedImage.Bounds().Max.Y < monitor.Height {
			y = monitor.Height - (((monitor.Height) - (decodedImage.Bounds().Max.Y)) / 2) - decodedImage.Bounds().Max.Y + monitor.PosY
		}

		if decodedImage.Bounds().Max.Y > monitor.Height {
			y = monitor.Height - (((decodedImage.Bounds().Max.Y) - (monitor.Height)) / 2) - monitor.Height + monitor.PosY
			canvasHeight = decodedImage.Bounds().Max.Y - (((decodedImage.Bounds().Max.Y) - (monitor.Height)) / 2)
		}
	}

	decodedImage, err = cutter.Crop(decodedImage, cutter.Config{
		Width:  monitor.Width,
		Height: monitor.Height,
		Mode:   cropMode,
	})
	if err != nil {
		return err
	}

	spMin := image.Point{X: x, Y: y}
	// max is bottom left points plus Width and Height
	spMax := image.Point{X: x + canvasWidth, Y: y + canvasHeight}
	// Create a rectangle the size of the monitor
	monitorRectangle := image.Rectangle{Min: spMin, Max: spMax}

	if overlay {
		overlay := gg.NewContextForImage(decodedImage)

		imgX, imgY := decodedImage.Bounds().Max.X, decodedImage.Bounds().Max.Y

		_, th := overlay.MeasureString(imageInfo.Description)

		if err := overlay.LoadFontFace(fontPath, float64(monitor.Height/60)); err != nil {
			return err
		}

		if imageInfo.Lightness == "Light" || imageInfo.Lightness == "White" {
			// Set text lightness to 0 (black)
			overlay.SetRGB(0, 0, 0)
		} else {
			// Set text lightness to 1 (white)
			overlay.SetRGB(1, 1, 1)
		}

		// Draw the description on the image, position and size based on image width and height
		overlay.DrawStringWrapped(imageInfo.Description, float64(imgX)-float64(imgX/32), float64(imgY)-float64(th*4), 1, 1, float64(imgX/4), float64(1.5), gg.AlignLeft)

		draw.Draw(drawImage, monitorRectangle, overlay.Image(), image.Point{X: 0, Y: 0}, draw.Over)
	} else {
		draw.Draw(drawImage, monitorRectangle, decodedImage, image.Point{X: 0, Y: 0}, draw.Over)
	}

	decodedImage = nil

	return nil
}
