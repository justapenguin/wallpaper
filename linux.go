// +build linux,!js

package wallpaper

/*
#cgo LDFLAGS: -lX11 -lXinerama
#include <X11/Xlib.h>
#include <X11/extensions/Xinerama.h>
*/
import "C"

import (
	"fmt"
	"os/exec"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
	"unsafe"

	"gopkg.in/ini.v1"
	"gopkg.in/yaml.v2"
)

func getMonitors() ([]*Monitor, error) {
	display := C.XOpenDisplay(nil)
	defer C.XCloseDisplay(display)

	var numDisplays C.int

	xineramaScreenInfo := C.XineramaQueryScreens(display, &numDisplays)
	defer C.XFree(unsafe.Pointer(xineramaScreenInfo))

	screens := (*[1 << 30]C.XineramaScreenInfo)(unsafe.Pointer(xineramaScreenInfo))[:numDisplays:numDisplays]

	monitors := make([]*Monitor, numDisplays)

	for i := 0; i < int(numDisplays); i++ {
		screen := C.XineramaScreenInfo(screens[i])

		monitors[i] = &Monitor{
			ID:     MonitorID(fmt.Sprintf("%d-%d-%d-%d-%d", screen.screen_number, screen.width, screen.height, screen.x_org, screen.y_org)),
			Width:  int(screen.width),
			Height: int(screen.height),
			PosX:   int(screen.x_org),
			PosY:   int(screen.y_org),
			Num:    int(screen.screen_number),
		}
	}

	if len(monitors) == 0 {
		return monitors, nil
	}

	minPosX, minPosY := monitors[0].PosX, monitors[0].PosY
	adjX, adjY := 0, 0

	for _, monitor := range monitors {
		if monitor.PosX <= minPosX {
			minPosX = monitor.PosX
			adjX = int(0 - minPosX)
		}

		if monitor.PosY <= minPosY {
			minPosY = monitor.PosY
			adjY = int(0 - minPosY)
		}
	}

	for _, monitor := range monitors {
		monitor.PosX += adjX
		monitor.PosY += adjY

		if monitor.Width == 0 || monitor.Height == 0 {
			continue
		}

		divisor := gcd(monitor.Width, monitor.Height)
		monitor.AspectX = monitor.Width / divisor
		monitor.AspectY = monitor.Height / divisor
	}

	return monitors, nil
}

// Get returns the current wallpaper.
func Get() (string, error) {
	if isGNOMECompliant() {
		return parseDconf("gsettings", "get", "org.gnome.desktop.background", "picture-uri")
	}

	switch Desktop {
	case "KDE":
		return parseKDEConfig()
	case "X-Cinnamon":
		return parseDconf("dconf", "read", "/org/cinnamon/desktop/background/picture-uri")
	case "MATE":
		return parseDconf("dconf", "read", "/org/mate/desktop/background/picture-filename")
	case "XFCE":
		output, err := exec.Command("xfconf-query", "-c", "xfce4-desktop", "-p", "/backdrop/screen0/monitor0/workspace0/last-image").Output()
		if err != nil {
			return "", err
		}
		return strings.TrimSpace(string(output)), nil
	case "LXDE":
		return parseLXDEConfig()
	case "Deepin":
		return parseDconf("dconf", "read", "/com/deepin/wrap/gnome/desktop/background/picture-uri")
	default:
		return "", ErrUnsupportedDE
	}
}

// SetFromFile sets wallpaper from a file path.
func SetFromFile(file string) error {
	if isGNOMECompliant() {
		return exec.Command("gsettings", "set", "org.gnome.desktop.background", "picture-uri", strconv.Quote("file://"+file)).Run()
	}

	switch Desktop {
	case "KDE":
		return setKDEBackground("file://" + file)
	case "X-Cinnamon":
		return exec.Command("dconf", "write", "/org/cinnamon/desktop/background/picture-uri", strconv.Quote("file://"+file)).Run()
	case "MATE":
		return exec.Command("dconf", "write", "/org/mate/desktop/background/picture-filename", strconv.Quote(file)).Run()
	case "XFCE":
		return exec.Command("xfconf-query", "-c", "xfce4-desktop", "-p", "/backdrop/screen0/monitor0/workspace0/last-image", "-s", file).Run()
	case "LXDE":
		return exec.Command("pcmanfm", "-w", file).Run()
	case "Deepin":
		return exec.Command("dconf", "write", "/com/deepin/wrap/gnome/desktop/background/picture-uri", strconv.Quote("file://"+file)).Run()
	default:
		return ErrUnsupportedDE
	}
}

// SetMultiWall sets each given image on the corresponding Monitor.
// If a certain wallpaper cannot be applied, an error is returned with the corresponding index
// so that it can be dealt with (e.g. removed and reattempted)
func SetMultiWall(monitors []*Monitor, imagesInfo []*MonitorImage, overlay bool) (int, error) {
	err := SetDisplayMode(Tile)

	if err != nil {
		return -1, err
	}

	multiWallPath, failedIndex, err := makeMultiWall(monitors, imagesInfo, overlay)

	if err != nil {
		return failedIndex, err
	}

	return -1, SetFromFile(multiWallPath)
}

func removeProtocol(input string) string {
	if len(input) >= 7 && input[:7] == "file://" {
		return input[7:]
	}
	return input
}

func parseDconf(command string, args ...string) (string, error) {
	output, err := exec.Command(command, args...).Output()
	if err != nil {
		return "", err
	}

	// unquote string
	var unquoted string
	// the output is quoted with single quotes, which cannot be unquoted using strconv.Unquote, but it is valid yaml
	err = yaml.UnmarshalStrict(output, &unquoted)
	if err != nil {
		return unquoted, err
	}

	return removeProtocol(unquoted), nil
}

func parseLXDEConfig() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}

	cfg, err := ini.Load(filepath.Join(usr.HomeDir, ".config/pcmanfm/LXDE/desktop-items-0.conf"))
	if err != nil {
		return "", err
	}

	key, err := cfg.Section("*").GetKey("wallpaper")
	if err != nil {
		return "", err
	}
	return key.String(), err
}

func isGNOMECompliant() bool {
	return strings.Contains(Desktop, "GNOME") || Desktop == "Unity" || Desktop == "Pantheon"
}

func SetDisplayMode(mode string) error {
	if isGNOMECompliant() {
		gnomeMode := "centered"

		switch mode {
		case Tile:
			gnomeMode = "wallpaper"
		case Center:
			gnomeMode = "centered"
		case Stretch:
			gnomeMode = "scaled"
		case Span:
			gnomeMode = "spanned"
		}

		return exec.Command("gsettings", "set", "org.gnome.desktop.background", "picture-options", gnomeMode).Run()
	}

	// @TODO other desktop environments
	return nil
}
