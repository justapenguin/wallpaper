// +build darwin,!js

package wallpaper

/*
#cgo CFLAGS: -x objective-c
#cgo LDFLAGS: -framework ApplicationServices -framework Foundation -framework AppKit

#import <Foundation/Foundation.h>
#import <ApplicationServices/ApplicationServices.h>
#import <AppKit/AppKit.h>
#import <AppKit/NSEvent.h>
#import <AppKit/NSScreen.h>

#include <ApplicationServices/ApplicationServices.h>

typedef struct {
	float width, height, posX, posY;
	uint32_t id, num;
} display_t;

// numDisplays finds the number of currently connected displays
uint32_t numDisplays() {
	uint32_t activeDisplayCount = 0;
	CGError rc;
	rc = CGGetActiveDisplayList(0, NULL, &activeDisplayCount);

	if (rc != kCGErrorSuccess) {
		return -1;
	}

	return activeDisplayCount;
}

// getDisplay returns a display_t for a given display number
display_t* getDisplay(uint32_t displayNum, uint32_t activeDisplayCount) {
	if (displayNum >= activeDisplayCount) {
		return NULL;
	}

	CGError rc;
	uint32_t displayCount = 0;
	CGDirectDisplayID *activeDisplays = NULL;

	activeDisplays = (CGDirectDisplayID *) malloc(activeDisplayCount * sizeof(CGDirectDisplayID));
	if (activeDisplays == NULL) {
		return NULL;
	}

	rc = CGGetActiveDisplayList(activeDisplayCount, activeDisplays, &displayCount);
	if (rc != kCGErrorSuccess) {
		return NULL;
	}

	display_t* display = malloc(sizeof(display_t));

	CGRect bounds = CGDisplayBounds(activeDisplays[displayNum]);

	display->width = bounds.size.width;
	display->height = bounds.size.height;
	display->posX = bounds.origin.x;
	display->posY = bounds.origin.y;
	display->num = displayNum;
	display->id = CGDisplaySerialNumber(activeDisplays[displayNum]);

	free(activeDisplays);

	return display;
}

void setWallpaper(char *str, int index) {
	NSString *filepath = [NSString stringWithUTF8String:str];

	NSWorkspace *ws = [NSWorkspace sharedWorkspace];
	NSArray *screens = [NSScreen screens];

	if (index >= [screens count]) {
	    return;
	}

	NSError *err;
	[ws setDesktopImageURL:[NSURL fileURLWithPath:filepath]
			   forScreen:screens[index]
			   options:[ws desktopImageOptionsForScreen:screens[index]]
			   error:&err];
}
*/
import "C"

import (
	"bufio"
	"errors"
	"fmt"
	"image"
	"image/jpeg"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"unsafe"
)

var errUnableToGetDisplay = errors.New("unable to get display information")

func getMonitors() ([]*Monitor, error) {
	numDisplays := C.numDisplays()

	if numDisplays <= 0 {
		return nil, errUnableToGetDisplay
	}

	monitors := make([]*Monitor, int(numDisplays))

	for i := 0; i < int(numDisplays); i++ {
		display := C.getDisplay(C.uint32_t(i), numDisplays)

		if display == nil {
			return nil, errUnableToGetDisplay
		}

		monitors[i] = &Monitor{
			ID:     MonitorID(fmt.Sprintf("%d", display.id)),
			Width:  int(display.width),
			Height: int(display.height),
			PosX:   int(display.posX),
			PosY:   int(display.posY),
			Num:    int(display.num),
		}

		C.free(unsafe.Pointer(display))
	}

	if len(monitors) == 0 {
		return monitors, nil
	}

	minPosX, minPosY := monitors[0].PosX, monitors[0].PosY
	adjX, adjY := 0, 0

	for _, monitor := range monitors {
		if monitor.PosX <= minPosX {
			minPosX = monitor.PosX
			adjX = int(0 - minPosX)
		}

		if monitor.PosY <= minPosY {
			minPosY = monitor.PosY
			adjY = int(0 - minPosY)
		}
	}

	for _, monitor := range monitors {
		monitor.PosX += adjX
		monitor.PosY += adjY

		if monitor.Width == 0 || monitor.Height == 0 {
			continue
		}

		divisor := gcd(monitor.Width, monitor.Height)
		monitor.AspectX = monitor.Width / divisor
		monitor.AspectY = monitor.Height / divisor
	}

	return monitors, nil
}

// Get returns the path to the current wallpaper.
func Get() (string, error) {
	stdout, err := exec.Command("osascript", "-e", `tell application "Finder" to get POSIX path of (get desktop picture as alias)`).Output()
	if err != nil {
		return "", err
	}

	// is calling strings.TrimSpace() necessary?
	return strings.TrimSpace(string(stdout)), nil
}

// SetFromFile uses AppleScript to tell Finder to set the desktop wallpaper to specified file.
func SetFromFile(file string) error {
	monitors, err := GetMonitors()

	if err != nil {
		return err
	}

	for _, monitor := range monitors {
		str := C.CString(file)
		C.setWallpaper(str, C.int(monitor.Num))
		C.free(unsafe.Pointer(str))
	}

	return nil
}

// SetMultiWall sets each given image on the corresponding Monitor.
// If a certain wallpaper cannot be applied, an error is returned with the corresponding index
// so that it can be dealt with (e.g. removed and reattempted)
func SetMultiWall(monitors []*Monitor, imagesInfo []*MonitorImage, overlay bool) (int, error) {
	for path := range previousTempPaths {
		err := os.Remove(path)

		if err != nil {
			return -1, err
		}

		delete(previousTempPaths, path)
	}

	for monitorIndex, monitor := range monitors {

		path, err := processMacOSMonitorImage(monitor, imagesInfo[monitorIndex], overlay)

		if err != nil {
			return monitorIndex, err
		}

		previousTempPaths[path] = true

		str := C.CString(path)
		C.setWallpaper(str, C.int(monitor.Num))
		C.free(unsafe.Pointer(str))
	}

	return -1, nil
}

func processMacOSMonitorImage(monitor *Monitor, info *MonitorImage, withOverlay bool) (string, error) {
	r := image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: monitor.Width, Y: monitor.Height}}
	rgba := image.NewRGBA(r)

	err := processMonitorImage(monitor, info, rgba, withOverlay)

	if err != nil {
		return "", err
	}

	// macOS cannot set the same wallpaper path twice, so we have to generate a random path
	// and remember it and then delete it next time SetMultiWall is called.
	path := getRandomTempPath()

	outImage, err := os.Create(path)

	if err != nil {
		return "", err
	}

	defer outImage.Close()

	output := bufio.NewWriter(outImage)
	err = jpeg.Encode(output, rgba, &jpeg.Options{Quality: 100})

	if err != nil {
		return "", err
	}

	return path, nil
}

var previousTempPaths = make(map[string]bool)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func getRandomTempPath() string {
	return filepath.Join(os.TempDir(), fmt.Sprintf("crop%s.jpg", RandStringBytes(10)))
}

func SetDisplayMode(mode string) error {
	return nil // no-op
}
