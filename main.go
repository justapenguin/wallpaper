package wallpaper

import (
	"os"
	"os/user"
	"path/filepath"
	"runtime"
	"sync"

	"github.com/pkg/errors"
)

// Desktop contains the current desktop environment on Linux.
// Empty string on all other operating systems.
var Desktop = os.Getenv("XDG_CURRENT_DESKTOP")

// ErrUnsupportedDE is thrown when Desktop is not a supported desktop environment.
var ErrUnsupportedDE = errors.New("your desktop environment is not supported")

var ErrDecodeImage = errors.New("can't decode the image")

const (
	None       = "None"
	Fill       = "Fill"
	Fit        = "Fit"
	Tile       = "Tile"
	Stretch    = "Stretch"
	Center     = "Center"
	Span       = "Span"
	FillCenter = "Fill and Center"
	FitCenter  = "Fit and Center"
)

type MonitorID string

func (m MonitorID) String() string {
	return string(m)
}

var getMonitorsMutex sync.Mutex

// GetMonitors returns size and position information about all known monitors.
func GetMonitors() ([]*Monitor, error) {
	getMonitorsMutex.Lock()
	defer getMonitorsMutex.Unlock()

	return getMonitors()
}

// Monitor contains information about a monitor
type Monitor struct {
	PosX, PosY       int
	AspectX, AspectY int
	Width, Height    int
	ID               MonitorID
	Active           bool

	Num int
}

// MonitorImage contains information about an image for a Monitor
type MonitorImage struct {
	ImagePath   string
	Mode        string
	Description string
	Lightness   string
}

func getCacheDir() (string, error) {
	switch runtime.GOOS {
	case "windows":
		return os.TempDir(), nil
	case "linux":
		usr, err := user.Current()
		if err != nil {
			return "", err
		}
		return filepath.Join(usr.HomeDir, ".cache"), nil
	case "darwin":

		usr, err := user.Current()
		if err != nil {
			return "", err
		}

		return filepath.Join(usr.HomeDir, "Library", "Caches"), nil

	default:
		return "", errors.New("unknown OS")
	}
}
