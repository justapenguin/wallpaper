// +build windows,!js

package wallpaper

import (
	"fmt"
	"sort"
	"strings"
	"syscall"
	"unicode/utf16"
	"unsafe"

	"github.com/pkg/errors"
	"golang.org/x/sys/windows/registry"
)

// https://msdn.microsoft.com/en-us/library/windows/desktop/ms724947.aspx
const (
	spiGetDeskWallpaper = 0x0073
	spiSetDeskWallpaper = 0x0014

	uiParam = 0x0000

	spifUpdateINIFile = 0x01
	spifSendChange    = 0x02
)

// https://msdn.microsoft.com/en-us/library/windows/desktop/ms724947.aspx
var (
	user32               = syscall.NewLazyDLL("user32.dll")
	systemParametersInfo = user32.NewProc("SystemParametersInfoW")
)

//setupSubKey contains information about a windows subkey
type setupSubKey struct {
	timeStamp uint64
	subKey    string
}

//ByTime implements sort.Interface for []setupSubKey based on the PosX field
type ByTime []*setupSubKey

func (a ByTime) Len() int           { return len(a) }
func (a ByTime) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByTime) Less(i, j int) bool { return a[i].timeStamp < a[j].timeStamp }

// Get returns the current wallpaper.
func Get() (string, error) {
	// the maximum length of a windows path is 256 utf16 characters
	var filename [256]uint16
	systemParametersInfo.Call(
		uintptr(spiGetDeskWallpaper),
		uintptr(cap(filename)),
		// the memory address of the first byte of the array
		uintptr(unsafe.Pointer(&filename[0])),
		uintptr(0),
	)
	return strings.Trim(string(utf16.Decode(filename[:])), "\x00"), nil
}

// SetFromFile sets the wallpaper for the current user.
func SetFromFile(filename string) error {
	filenameUTF16, err := syscall.UTF16PtrFromString(filename)
	if err != nil {
		return err
	}

	systemParametersInfo.Call(
		uintptr(spiSetDeskWallpaper),
		uintptr(uiParam),
		uintptr(unsafe.Pointer(filenameUTF16)),
		uintptr(spifUpdateINIFile|spifSendChange),
	)
	return nil
}

const winOverflowLimit int64 = 0x0fffffff
const winOverflowAdjust int64 = 0xffffffff

func getMonitors() ([]*Monitor, error) {
	//Open the registry key that stores monitor information
	//0x20019 is KEY_READ access, combining STANDARD_RIGHTS_READ, KEY_QUERY_VALUE, KEY_ENUMERATE_SUB_KEYS, and KEY_NOTIFY values.
	k, err := registry.OpenKey(registry.LOCAL_MACHINE, `SYSTEM\CurrentControlSet\Control\GraphicsDrivers\Configuration`, 0x20019)
	if err != nil {
		return nil, errors.Wrap(err, "open registry key failed")
	}
	defer k.Close()
	subKeys, err := getSubKeys(k)
	if err != nil {
		return nil, errors.Wrap(err, "get registry subkeys failed")
	}
	var setupSubKeys []*setupSubKey
	//For the range of subkeys get the timestamp and ID, and save in a slice of monitorSubKey structs
	//Could move the contents of this for loop to a function, allowing for defer l.Close()
	for _, subKey := range subKeys {
		l, err := registry.OpenKey(registry.LOCAL_MACHINE, fmt.Sprintf(`SYSTEM\CurrentControlSet\Control\GraphicsDrivers\Configuration\%s`, subKey), 0x20019)
		if err != nil {
			return nil, errors.Wrap(err, "open registry key failed")
		}
		timeStamp, _, err := l.GetIntegerValue("Timestamp")
		if err != nil {
			return nil, errors.Wrap(err, "load registry value failed")
		}
		setupSubKeys = append(setupSubKeys, &setupSubKey{
			timeStamp: timeStamp,
			subKey:    subKey,
		})
		l.Close()
	}

	//Find the member of the slice with the most recent timestamp and get its key
	sort.Sort(ByTime(setupSubKeys))

	monitorKey := setupSubKeys[len(setupSubKeys)-1]

	m, err := registry.OpenKey(registry.LOCAL_MACHINE, fmt.Sprintf(`SYSTEM\CurrentControlSet\Control\GraphicsDrivers\Configuration\%s`, monitorKey.subKey), 0x20019)
	if err != nil {
		return nil, errors.Wrap(err, "open registry key failed")
	}

	monitorSubKeys, err := getSubKeys(m)
	if err != nil {
		return nil, errors.Wrap(err, "get registry subkeys failed")
	}

	var monitors []*Monitor

	//For each monitor subkey, get the position and dimensions
	for _, monitorSubKey := range monitorSubKeys {
		o, err := registry.OpenKey(registry.LOCAL_MACHINE, fmt.Sprintf(`SYSTEM\CurrentControlSet\Control\GraphicsDrivers\Configuration\%s\%s`, monitorKey.subKey, monitorSubKey), 0x20019)
		if err != nil {
			return nil, errors.Wrap(err, "open registry key failed")
		}

		posX64, _, err := o.GetIntegerValue("Position.cx")
		if err != nil {
			return nil, errors.Wrap(err, "load registry value failed")
		}
		posX := int(posX64)

		posY64, _, err := o.GetIntegerValue("Position.cy")
		if err != nil {
			return nil, errors.Wrap(err, "load registry value failed")
		}
		posY := int(posY64)

		width64, _, err := o.GetIntegerValue("PrimSurfSize.cx")
		if err != nil {
			return nil, errors.Wrap(err, "load registry value failed")
		}
		width := int(width64)

		height64, _, err := o.GetIntegerValue("PrimSurfSize.cy")
		if err != nil {
			return nil, errors.Wrap(err, "load registry value failed")
		}
		height := int(height64)

		//Find the greatest common divisor
		result := gcd(width, height)
		//Get the aspect ratio of the monitor
		aspectX := width / result
		aspectY := height / result

		//Append the monitor data to the monitor struct slice
		monitors = append(monitors, &Monitor{
			PosX:    posX,
			PosY:    posY,
			AspectX: aspectX,
			AspectY: aspectY,
			Width:   width,
			Height:  height,
			ID:      "",
		})
	}

	for _, monitor := range monitors {
		//If position has wrapped around (if a monitor is on the left or above) then find the correct (negative!) number
		if int64(monitor.PosX) > winOverflowLimit {
			//Gives the positive value of position
			newPos := -(winOverflowAdjust - int64(monitor.PosX) + 1)
			monitor.PosX = int(newPos)
		}

		if int64(monitor.PosY) > winOverflowLimit {
			newPos := -(winOverflowAdjust - int64(monitor.PosY) + 1)

			monitor.PosY = int(newPos)
		}
	}

	minPosX, minPosY := monitors[0].PosX, monitors[0].PosY

	for _, monitor := range monitors {
		if monitor.PosX <= minPosX {
			minPosX = monitor.PosX
		}

		if monitor.PosY <= minPosY {
			minPosY = monitor.PosY
		}
	}

	for index, monitor := range monitors {
		monitor.PosX -= minPosX
		monitor.PosY -= minPosY

		//Create a unique id for each monitor using dimensions and position
		monitor.ID = MonitorID(fmt.Sprintf("%d-%d-%d-%d", monitor.Width, monitor.Height, monitor.PosX, monitor.PosY))
		monitor.Num = index
	}

	return monitors, nil
}

// SetMultiWall sets each given image on the corresponding Monitor.
// If a certain wallpaper cannot be applied, an error is returned with the corresponding index
// so that it can be dealt with (e.g. removed and reattempted)
func SetMultiWall(monitors []*Monitor, imagesInfo []*MonitorImage, overlay bool) (int, error) {
	err := SetDisplayMode(Tile)

	if err != nil {
		return -1, err
	}

	multiWallPath, failedIndex, err := makeMultiWall(monitors, imagesInfo, overlay)

	if err != nil {
		return failedIndex, err
	}

	return -1, SetFromFile(multiWallPath)
}

func getSubKeys(k registry.Key) ([]string, error) {
	//Get the number of subkeys (These are time stamped monitor formats)
	keyInfo, err := k.Stat()
	if err != nil {
		return nil, err
	}
	n32 := keyInfo.SubKeyCount
	n := int(n32)
	//Get the names of all subkeys
	subKeys, err := k.ReadSubKeyNames(n)
	if err != nil {
		return nil, err
	}
	return subKeys, nil
}

//Set the windows display mode based on a constant string
func SetDisplayMode(mode string) error {
	//Open the registry key
	o, err := registry.OpenKey(registry.CURRENT_USER, `Control Panel\Desktop`, 0x2003B)
	if err != nil {
		return errors.Wrap(err, "open registry key failed")
	}
	//Check if tiled mode is enabled
	tiled, _, err := o.GetStringValue("TileWallpaper")
	if err != nil {
		return errors.Wrap(err, "load registry value failed")
	}
	//Get the current mode
	modeReg, _, err := o.GetStringValue("WallpaperStyle")
	if err != nil {
		return errors.Wrap(err, "load registry value failed")
	}

	switch mode {
	case Tile:
		//If tiled mode is not already set then set it
		if tiled != "1" {
			err := o.SetStringValue("TileWallpaper", "1")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}

			err = o.SetStringValue("WallpaperStyle", "0")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}
		}
	case Fill:
		if modeReg != "10" {
			err = o.SetStringValue("WallpaperStyle", "10")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}

			err := o.SetStringValue("TileWallpaper", "0")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}
		}
	case Fit:
		if modeReg != "6" {
			err = o.SetStringValue("WallpaperStyle", "6")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}

			err := o.SetStringValue("TileWallpaper", "0")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}
		}
	case Stretch:
		if modeReg != "2" {
			err = o.SetStringValue("WallpaperStyle", "2")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}

			err := o.SetStringValue("TileWallpaper", "0")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}
		}
	case Center:
		if modeReg != "0" {
			err = o.SetStringValue("WallpaperStyle", "0")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}

			err := o.SetStringValue("TileWallpaper", "0")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}
		}
	case Span:
		if modeReg != "22" {
			err = o.SetStringValue("WallpaperStyle", "22")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}

			err := o.SetStringValue("TileWallpaper", "0")
			if err != nil {
				return errors.Wrap(err, "update registry value failed")
			}
		}
	}
	return nil
}
